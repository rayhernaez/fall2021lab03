package LinearAlgebra;

public class test {
    public static void main(String[] args) {

        Vector3d newVector = new Vector3d(7,5,3);

        System.out.println(newVector.getX());
        System.out.println(newVector.getY());
        System.out.println(newVector.getZ());

        System.out.println(newVector.magnitude());
        System.out.println(newVector.dotProduct(1,2,3));
        System.out.println(newVector.add(1,2,3));

    }

}
