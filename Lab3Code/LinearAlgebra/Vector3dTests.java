package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

/*
* Ray Hernaez
* 1241417
*/

public class Vector3dTests {
    
    Vector3d v1 = new Vector3d(7, 5, 3);

    @Test
    public void testingX() {
        assertEquals(v1.getX(), 7);
    }

    @Test
    public void testingY() {
        assertEquals(v1.getY(), 5);
    }

    @Test
    public void testingZ() {
        assertEquals(v1.getZ(), 3);
    }

    @Test
    public void testingMagnitude() {
        assertEquals(v1.magnitude(), 9.1104335791443);
    }

    @Test
    public void testingDotProduct() {
        assertEquals(v1.dotProduct(1,2,3), 26);
    }

    //Not sure about this...?
    @Test
    public void testingAdd() {
        Vector3d v2 = v1.add(1,2,3);
        Vector3d v3 = v2;
        assertEquals(v2, v3);

        /*
        Vector3d v2 = new Vector3d(8,7,5);
        assertEquals(v1.add(1,2,3), v2); //Not Equal b/c point to two diff. Objects

        assertEquals(v1.add(1,2,3), "(8.0, 7.0, 6.0)"); //Can't compare Object and String
        */
    }

}
