package LinearAlgebra;
import java.lang.Math;

/*
* Ray Hernaez
* 1241417
*/

public class Vector3d {

    private double x;
    private double y;
    private double z;

    //constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //getters
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double magnitude;
        magnitude = Math.sqrt((Math.pow(this.x, 2) + (Math.pow(this.y, 2) + (Math.pow(this.z, 2)))));
        return magnitude;
    }

    public double dotProduct(double a, double b, double c) {
        double dotProduct;
        dotProduct = ((this.x * a) + (this.y * b) + (this.z * c));
        return dotProduct;
    }

    public Vector3d add(double a, double b, double c) {
        double x, y, z;

        x = (this.x + a);
        y = (this.y + b);
        z = (this.z + c);

        Vector3d newVector = new Vector3d(x,y,z);
        return newVector;
    }

    //toString
    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

}